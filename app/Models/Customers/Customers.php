<?php

namespace App\Models\Customers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Customers extends Model
{

    protected $table = 'customer';
    protected $customersHelper;

    use HasFactory;
}

