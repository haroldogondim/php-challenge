<?php

namespace App\Helpers;

class PhoneHelpers {

    private $countriesByRegex = [
        'Cameroon' => '/(\(237\)\ ?)([2368]\d{7,8})$/',
        'Ethiopia' => '/(\(251\)\ ?)([1-59]\d{8})$/',
        'Morocco' => '/(\(212\)\ ?)([5-9]\d{8})$/',
        'Mozambique' => '/(\(258\)\ ?)([28]\d{7,8})$/',
        'Uganda' => '/(\(256\)\ ?)(\d{9})$/'
    ];

    public array $countryByCode = [
        '237' => 'Cameroon',
        '251' => 'Ethiopia',
        '212' => 'Morocco',
        '258' => 'Mozambique',
        '256' => 'Uganda'
    ];

    private $phoneAndCodeRegex = '/\(([0-9]+ ?)\)(.+)$/';

    public function isValidPhoneNumber($phone) {
        foreach($this->countriesByRegex as $regex) {
            if(preg_match($regex, $phone, $matches)) {
                return true;
            }
        }

        return false;
    }

    public function getSlicedPhoneNumber($phone) {
        if(preg_match($this->phoneAndCodeRegex, $phone, $matches)) {
            $data = new \stdClass();
            $data->code = trim($matches[1]);
            $data->number = trim($matches[2]);
            //$data = ['code' => trim($matches[1]), 'number' => trim($matches[2])];
            return $data;
        }

        return null;
    }
}