<?php

namespace App\Helpers;

use App\Helpers\PhoneHelpers;

class CustomersHelper {

    private $phoneHelpers; 

    function __construct(PhoneHelpers $phoneHelpers) {
        $this->phoneHelpers = $phoneHelpers;
    }

    public function getFormattedCustomers($customers) {
        foreach($customers as $index => $row) {
            $slicedPhone = $this->phoneHelpers->getSlicedPhoneNumber($row->phone);
            if($this->phoneHelpers->countryByCode[$slicedPhone->code]) {
                $customers[$index]->country = $this->phoneHelpers->countryByCode[$slicedPhone->code];
            }
            $customers[$index]->state = $this->phoneHelpers->isValidPhoneNumber($row->phone);
            $customers[$index]->slicedPhone = $slicedPhone;
        }

        return $customers;
    }

    public function getCustomersByCountry($customers, $country) {
        $temp = [];
        foreach($customers as $index => $customer) {
            if($customer->country == $country) {
                $temp[$index] = $customer;
            }
        }

        return (object) $temp;
    }

    public function getCustomersByState($customers, $state) {
        $temp = [];
        foreach($customers as $index => $customer) {
            $customerState = (bool) $customer->state;
            if($customerState === (bool) $state) {
                $temp[$index] = $customer;
            }
        }

        return (object) $temp;
    }

}