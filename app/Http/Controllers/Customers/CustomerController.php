<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customers\Customers;
use App\Helpers\CustomersHelper;
use App\Helpers\PhoneHelpers;

class CustomerController extends Controller
{

    public function index(Customers $customers, CustomersHelper $customerHelper, PhoneHelpers $phoneHelper) {
        $customers = $customers::all();
        $customers = $customerHelper->getFormattedCustomers($customers);
        $getCountries = array_values($phoneHelper->countryByCode);
        return view('app.index', ['customers' => $customers, 'countries' => $getCountries]);
    }

    public function customersWithFilter(Customers $customers, CustomersHelper $customerHelper, PhoneHelpers $phoneHelper, Request $request) {
        $customers = $customers::all();
        $customers = $customerHelper->getFormattedCustomers($customers);
        $country = $request->input('country') ?? $request->input('country');
        $state = $request->input('state') ?? $request->input('state');
        if($request->isMethod('post')) {
            if($country && !empty($country)) {
                $customers = $customerHelper->getCustomersByCountry($customers, $country);
            }

            if($state && !empty($state)) {
                $stateField = $request->input('state') == 'valid' ? true : false;
                $customers = $customerHelper->getCustomersByState($customers, $stateField);
            }
        }

        $getCountries = array_values($phoneHelper->countryByCode);
        return view('app.index', ['customers' => $customers, 'countries' => $getCountries]);
    }
}
