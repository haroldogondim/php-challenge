<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jumia Challenge</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css">
    
</head>
<body>
    <div class="container">
        <div class="row mt-5">
            <h2>Phone numbers</h2>
        </div>
        <form action="{{ route('app.customersWithFilter') }}" method="POST">
            @csrf
            <div class="row mt-5">
                <div class="col-md-4">
                    <select class="form-select" class="country" name="country" id="country">
                        <option value="">Select country</option>
                        @foreach($countries as $country)
                        <option value="{{ $country }}"@if(request()->get('country') == $country) selected @endif>{{ $country }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <select class="form-select" class="phone_valid" name="state" id="state">
                        <option value="">Select number state</option>
                        <option value="valid"@if(request()->get('state') == 'valid') selected @endif>Valid phone numbers</option>
                        <option value="invalid"@if(request()->get('state') == 'invalid') selected @endif>Invalid phone numbers</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-dark">Send</button>
                </div>
            </div>
        </form>
        <div class="row mt-5">
            <table class="table" id="phone-data">
                <thead>
                    <tr>
                        <th scope="col">Country</th>
                        <th scope="col">State</th>
                        <th scope="col">Country code</th>
                        <th scope="col">Phone num.</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($customers as $customer)
                    <tr>
                    
                        <td>{!! $customer->country ? $customer->country : '<i>Undefined</i>' !!}</td>
                        <td>{{ $customer->state ? 'OK' : 'NOK' }}</td>
                        <td>+{{ $customer->slicedPhone->code }}</td>
                        <td>{{ $customer->slicedPhone->number }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="row mt-2">
            <div class="d-flex justify-content-center">
            </div>
        </div>
    </div>    
</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
<script src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jumia.js"></script>
</html>